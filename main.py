from flask import Flask, request, jsonify
import threading
from llama_query import completion

app = Flask(__name__)
app.config['TIMEOUT'] = 600 
class QueryLLaMa:
    def __init__(self, prompt, temperature=None, model=None):
        self.prompt = prompt
        self.temperature = temperature
        self.model = model

def perform_completion(query):
    result = completion(query.prompt)
    response = {
        "object": "chat.completion",
        "model": "",
        "usage": result['usage'],
        'choices': result['choices'],
    }
    # Here you can add any additional logic that should happen after the completion
    return response

@app.route("/api/v1/complete", methods=["POST"])
def completion_chat():
    query_data = request.json
    query = QueryLLaMa(**query_data)
    response = perform_completion(query)
    return jsonify(response)

@app.route("/")
def root():
    return {"message": "Llama API"}

if __name__ == "__main__":
    app.run(debug=True)
