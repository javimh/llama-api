# llama-api
Basic api using flask and llama 2 models without CUDA or OpenCL.
 
## Installation
```bash
python -m venv env
source env/bin/activate
pip install -r requirements.txt
python main.py
```

## Example request

`POST localhost:5000/api/v1/complete `  
Body: 
```JAVASCRIPT
{
	"prompt":"generate an example of api in python using flask with a get endpoint and post"
}
```
## License 
This code is published under META Llama 2 license for non commercial uses. Copyright Javier Felix Montes Heras